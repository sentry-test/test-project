import { Component } from '@angular/core';

class CustomError2 extends Error {}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-test';

  errorOut() {
    console.error('errorrrrr123');
    throw new CustomError2('Hallo Niclas!');
  }
}
